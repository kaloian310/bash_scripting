import SwiftUI 
// MARK: - Models 
struct BudgetItem: Identifiable { 
    let id = UUID() 
    var name: String 
    var amount: Double 
    var category: Category 
    var date: Date 
    
    enum Category: String, CaseIterable 
    { 
        case income, expense 
        }     
    } 
    
// MARK: - ViewModels 
    
class BudgetViewModel: ObservableObject 
{ 
    @Published var budgetItems: [BudgetItem] = [] 
        
    var totalIncome: Double { 
        budgetItems.filter { $0.category == .income }.reduce(0) { $0 + $1.amount } 
        } 
            
    var totalExpense: Double { 
        budgetItems.filter { $0.category == .expense }.reduce(0) { $0 + $1.amount } 
        } 
        
    var balance: Double { 
        totalIncome - totalExpense 
        } 
        
    func addItem(_ item: BudgetItem) { budgetItems.append(item) 
    } 
        
} 
    
// MARK: - Views 
    
struct ContentView: View { 
    @StateObject private var viewModel = BudgetViewModel() 
    @State private var showingAddItemView = false 
    
    var body: some View { 
        NavigationView {
             List { 
                Section(header: Text("Overview")) 
                { HStack {
                     Text("Income:") 
                     Spacer() 
                     Text("$\\(viewModel.totalIncome, specifier: "%.2f")") 
                     } 
                    
                    HStack { 
                        Text("Expenses:") 
                        Spacer() 
                        Text("$\\(viewModel.totalExpense, specifier: "%.2f")") 
                    } 
                    
                    HStack { 
                        Text("Balance:") 
                        Spacer() 
                        Text("$\\(viewModel.balance, specifier: "%.2f")") 
                        .foregroundColor(viewModel.balance >= 0 ? .green : .red) 
                        } 
                } 
                
                Section(header: Text("Budget Items")) { 
                    ForEach(viewModel.budgetItems) { item in 
                        HStack { 
                            Text(item.name) 
                            Spacer() 
                            Text("$\\(item.amount, specifier: "%.2f")") 
                            .foregroundColor(item.category == .income ? .green : .red) 
                            } 
                        } 
                    } 
                } 
                
                .navigationTitle("Budget") 
                .toolbar {
                     ToolbarItem(placement: .navigationBarTrailing) { 
                        Button(action: { showingAddItemView = true }) {
                             Image(systemName: "plus") 
                             } 
                            } 
                        } 
                        .sheet(isPresented: $showingAddItemView) {
                             AddItemView(viewModel: viewModel) } 
            } 
        }
} 
                        
struct AddItemView: View { 
    @ObservedObject var viewModel: BudgetViewModel 
    @Environment(\\.presentationMode) var presentationMode 
    @State private var name = "" 
    @State private var amount = "" 
    @State private var category = BudgetItem.Category.expense 
    @State private var date = Date() 
    
    var body: some View { 
        NavigationView { 
            Form { 
                TextField("Name", text: $name) 
                TextField("Amount", text: $amount) 
                .keyboardType(.decimalPad) 
                Picker("Category", selection: $category) { 
                    ForEach(BudgetItem.Category.allCases, id: \\.self) { 
                        category in Text(category.rawValue.capitalized) } 
                } 
                    DatePicker("Date", selection: $date, displayedComponents: .date) 
            } 
                .navigationTitle("Add Budget Item") 
                .toolbar { ToolbarItem(placement: .navigationBarTrailing) { 
                    Button("Save") { 
                        if let amount = Double(amount) { 
                            let newItem = BudgetItem(name: name, amount: amount, category: category, date: date) 
                            viewModel.addItem(newItem) 
                            presentationMode.wrappedValue.dismiss() 
                                } 
                            } 
                        } 
                } 
         }
     } 
} 

// MARK: - Preview 

struct ContentView_Previews: PreviewProvider { 
    static var previews: some View {
         ContentView() 
        } 
}