# .bashrc
# Source global definitions

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=




# User specific aliases and functions
alias df='df -h'
alias ..='cd ..'

update () {
echo "Starting full system update..."
sudo dnf update
sudo dnf upgrade
sudo dnf autoremove
echo "Update Complete!"
return
}
export -f update

apps(){
echo "Installing Default app package... "
sudo dnf install vim
sudo dnf install screenfetch
sudo dnf install thunderbird
sudo dnf install corebird
sudo dnf install gimp
sudo dnf install vlc
echo "Packages Installed!"
}
export -f apps




