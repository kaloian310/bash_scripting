## Mac OS X Software Update via Terminal

To get a list of available software updates, type the following command:

`softwareupdate -l`

You will see a list of available updates. You can then install all available software updates with the following command:

`sudo softwareupdate -iva`

The use of sudo is required to get superuser privileges to actually install the updates. You can also install only the recommended updates with:

`sudo softwareupdate -irv`

Or you can just install specific software updates by specifying the shorthand package name from the previous list, like so:

`sudo softwareupdate -i iPhoneConfigurationUtility-3.2`

If there are any updates you want to ignore, you can do so with the `-–ignore` flag, for example:

`sudo softwareupdate --ignore iWeb3.0.2-3.0.2`

If you want to see all the available command line shortcuts for Software Update, just type:

`softwareupdate -h`

This is really useful for remotely updating Macs, setting up automated updates via a bash script, or if you just want to geek out.