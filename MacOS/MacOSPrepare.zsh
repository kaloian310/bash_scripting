#!/bin/zsh

# Preping the instalation requerments
echo "prepare the env.."
xcode-select --install
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Download HomeBrew pkg manager
brew install grep 

# Add the path to the shell file
echo 'export PATH="/opt/homebrew/opt/grep/libexec/gnubin:$PATH"' >> ~/.zshrc

# reloading .zshrc
echo "reloading .zshrc" | source ~/.zshrc

# KB tool Obsidian Download
brew install Obsidian --cask 

# Craft KB tool Download
brew install craft --cask

# VScode Download
echo "visual studio code installing..."
brew install visual-studio-code --cask


# Download Alfred search
echo "installing Alfred..."
brew install alfred --cask

# Grammerly Download
echo "Installing Grammerly..."
brew install grammarly --cask


# Download windows RDP
echo "Installing RDP oficial for Windows..."
brew install windows-app --cask


# Download ARC browser
curl -O https://releases.arc.net/release/Arc-latest.dmg
hdiutil attach ./Arc-latest.dmg
cp -R /Volumes/Arc/*.app /Applications/
hdiutil detach /Volumes/Arc
rm Arc-latest.dmg 


# Shottr app Download
echo "Screenshot app Shottr installing..."
curl -O https://shottr.cc/dl/Shottr-1.8.0.dmg
hdiutil attach ./Shottr-1.8.0.dmg
cp -R /Volumes/Shottr/*.app /Applications/
hdiutil detach /Volumes/Shottr


#CyberDuck Download
echo "Installing CyberDuck FTP..."
curl -O https://update.cyberduck.io/Cyberduck-9.0.2.42108.zip
unzip Cyberduck-9.0.2.42108.zip
cp -R Cyberduck.app /Applications/

# Slack Download
echo "Installing Slack Desktop..."
brew install slack --cask

# Warp Terminal Download
echo "Warp Terminal app..."
brew install --cask warp
















