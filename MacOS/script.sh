#!/bin/bash

echo 'script initiating...'
echo 'Please chose an action from the list below'
echo '1: Backup everything!'
echo '2: Backup current user files/folders'
echo '3: Update and Upgrade pkg'
echo '4: Restore from a backup'
echo '5: Fix file/folder names and permisions'
read
case $REPLY -eq 1 in
  local OF=Whole_System_$(date +%Y%m%d).tar.gz
  if [ -e $OF ]; then
	echo "File exists"
else
	echo "File does not exists"
fi
  sudo rm -rf /home/.../Backups/System/
  tar -zcvf $OF /
    ;; )
esac
case $REPLY -eq 2 in
  local OF=My_Files_$(date +%Y%m%d).tar.gz
  sudo rm -rf ~*/Backups/Files/
  tar -zcvf $OF ~/home )
    ;;
esac
case [ $REPLY -eq 3 ] in
  sudo apt-get update && sudo apt-get upgrade && apt-get autoremove )
    ;;
esac
case [ $REPLY -eq 4 ] in
  echo -e 'where is the file located?'
  read PATH
  if [[ $PATH='Whole_System_{*}' ]]; then
    sudo tar -xvf ~/Whole_System_{*} -C /Backups/System
  else
    sudo tar -xvf ~/My_Files_{*} -C /Backups/Files
  fi
  done )
    ;;
esac
case [ $REPLY -eq 5 ] in
  # This bash script will locate and replace spaces in the filenames
  # Controlling a loop with bash read command by redirecting STDOUT as a STDIN to while loop
  # find will not truncate filenames containing spaces
  # using POSIX class [:space:] to find space in the filename
  # substitute space with "_" character and consequently rename the file
  # fix all permisions
DIR="~/home"
find $DIR -type f | while read file; do
  if [[ "$file" = *[[:space:]]* ]]; then
    mv "$file" `echo $file | tr ' ' '_'`
  fi;
done

find $DIR -type d | while [[ read file ]]; do
  if [[ "$file" = *[[:space:]]* ]]; then
    mv "$file" `echo $file | tr ' ' '_'`
  fi;
done

chmod -R -type d 755
chmod
    )
    ;;
esac
case $REPLY -ne 1 | 2 | 3 | 4 | 5 in
  echo 'You need to chose one of the provided options above!!' )
    ;;
esac
echo 'Thank you for using our script, Bye!'
