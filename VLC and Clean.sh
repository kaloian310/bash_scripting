#!/bin/bash

# Base filename
base_filename="vlc_and_cleanup"

# Function to get the next available version number
get_next_version() {
    local version=1
    while [[ -f "${base_filename}_v${version}" ]]; do
        ((version++))
    done
    echo $version
}

# Get the next version number
version=$(get_next_version)

# Create the filename with version
filename="${base_filename}_v${version}"

# Export the script to the versioned file
cat << 'EOF' > "$filename"

#!/bin/bash

# Function to install pv if not present
install_pv() {
    if ! command -v pv &> /dev/null; then
        echo "Installing pv for progress bars..."
        sudo apt install pv -y
    fi
}

# Install pv
install_pv

# Search for the VLC package
echo "Searching for VLC package..."
VLC_SEARCH=$(apt search vlc 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$VLC_SEARCH"

# Install VLC
echo "Installing VLC..."
VLC_INSTALL=$(sudo apt install vlc -y 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$VLC_INSTALL"

# Update the system
echo "Updating system..."
UPDATE_OUTPUT=$(sudo apt update 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$UPDATE_OUTPUT"

# Print current kernel version after update
echo "Current kernel version after update:"
uname -r

# Upgrade packages
echo "Upgrading packages..."
UPGRADE_OUTPUT=$(sudo apt upgrade -y 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$UPGRADE_OUTPUT"

# Print current kernel version after upgrade
echo "Current kernel version after upgrade:"
uname -r

# Remove unused dependencies
echo "Removing unused dependencies..."
AUTOREMOVE_OUTPUT=$(sudo apt autoremove -y 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$AUTOREMOVE_OUTPUT"

# Clean apt cache
echo "Cleaning apt cache..."
CLEAN_OUTPUT=$(sudo apt clean 2>&1 | pv -p -t -e -w 80 > /dev/null)
echo "$CLEAN_OUTPUT"

# Remove obsolete packages
echo "Removing obsolete packages..."
OBSOLETE_PACKAGES=$(dpkg -l | awk '/^rc/ {print $2}')
if [ -n "$OBSOLETE_PACKAGES" ]; then
    REMOVE_OUTPUT=$(echo "$OBSOLETE_PACKAGES" | xargs sudo apt remove -y 2>&1 | pv -p -t -e -w 80 > /dev/null)
    echo "$REMOVE_OUTPUT"
else
    echo "No obsolete packages found."
fi

echo "All tasks completed!"
EOF