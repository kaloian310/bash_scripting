#!/bin/bash  
# -----------------------------------
# Author:	Kaloyan 
# Title:	GIT Script
# Description:	My own script for Creating a new Folder with .Git installed + npm for HTMX development or React
# -----------------------------------
echo "This is the Script for Adding new Projects to our GitLab acc"
echo "Chose a 'NAME' for your new Project:"
read PROJECT
NewFolder=$(mkdir $PROJECT) && Outputh=$(sudo pwd)
cd $Outputh/$PROJECT && echo "==> New folder was created and script is running inside the folder"
	if [[ $NewFolder == 0 ]]; 
	then  # Not Working Check for Error
		echo "==> Project with this name EXISTS, change the name!"
		exec "$0"
	fi
ChechOS=$(uname -s) 
echo "==> Your System is '$ChechOS', Continuing Script based on this Info"
if [[ $ChechOS == "Darwin" ]]; 
	then
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" 	
		brew install git
		git init
		echo "==> Enter remote Repository URL:"
		read REPO 
		git remote add origin $REPO 
		brew install node
		npm i -D
		echo "==> chose React APP Name:  * name can only contain URL-friendly characters and can no longer contain capital letters"
		read APP_NAME
		npx create-react-app {{$APP_NAME}}
		echo "==> New Project $PROJECT was created *Sucsesfully*, Git version controll was installed conected with the Tengri Solutiuons GitLab, additionally Node.JS and React.js were installed" 
	fi
	else
		sudo apt install git -y
		git init
		echo "==> Enter remote Repository URL:"
		read REPO2
		git remote add origin $REPO2
		curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
		sudo apt-get install -y nodejs
		npm i -D
		echo "==> Choose React APP Name:"
		read APP_NAME2
		npx create-react-app $APP_NAME2
		echo "==> New Project $PROJECT was created successfully. Git version control was installed and connected with the Tengri Solutions GitLab. Additionally, Node.js and React.js were installed."
fi